package com.mercury.test;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ImgBBUpload {
	
	public void imageLaunch() throws IOException{
		System.setProperty("webdriver.chrome.driver", "F:\\Palliumskills\\AutomationTesting\\Tools\\NewChrom\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.get("https://imgbb.com/");
		driver.manage().window().maximize();
		
		WebElement uName=driver.findElement(By.xpath(".//*[@id='home-cover-content']/div[2]/a"));
		uName.click();
		
		Runtime.getRuntime().exec("F:\\Palliumskills\\AutomationTesting\\AutoIT\\Script3.exe");	
	}

		public static void main(String[] args) throws IOException {
			ImgBBUpload obj=new ImgBBUpload();
			obj.imageLaunch();

	}

}
