package com.mercury.test;

import java.awt.AWTException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

//import com.gargoylesoftware.htmlunit.javascript.host.Screen;

public class MethodRepository {
	WebDriver driver;
	
	public void mercuryAppLaunch() throws InterruptedException, IOException
	{
		System.setProperty("webdriver.chrome.driver", "F:\\Palliumskills\\AutomationTesting\\Tools\\NewChrom\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("http://newtours.demoaut.com/");
		
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File("F:\\Palliumskills\\AutomationScreenshot\\screenshot01.png"));
		
		driver.manage().window().maximize();
	}
	
	public void login() throws InterruptedException, IOException, AWTException, FindFailed
	{
//WebElement uName= driver.findElement(By.name("userName"));
		WebElement uName= driver.findElement(By.xpath("//input[@name='userName']"));
		
				
		BufferedReader b1=new BufferedReader(new FileReader("./TestData.properties"));
		Properties p1=new Properties();
		p1.load(b1);
		
		String username=p1.getProperty("Username");
		String userpassword=p1.getProperty("Password");
		
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File("F:\\Palliumskills\\AutomationScreenshot\\screenshot02.png"));
		
		uName.sendKeys(username);
		WebElement uPassword= driver.findElement(By.name("password"));
		uPassword.sendKeys(userpassword);
  		
		File scrFile1 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile1, new File("F:\\Palliumskills\\AutomationScreenshot\\screenshot03.png"));
		
		
		//Screen scr=new Screen();
		//Pattern ptr = new Pattern("F:\\Palliumskills\\Sniping_tool\\Capture.png");
		//scr.click(ptr);
         
		
		//Robot R1=new Robot();
		//R1.keyPress(KeyEvent.VK_TAB);
		//R1.keyRelease(KeyEvent.VK_TAB);
		//R1.keyPress(KeyEvent.VK_ENTER);
		//R1.keyRelease(KeyEvent.VK_ENTER);
		
		
		WebElement uSign= driver.findElement(By.name("login"));
		//uSign.click();
		Thread.sleep(5000);
		
		Actions action = new Actions(driver);
		action.moveToElement(uSign).click().perform();
		
		WebElement element=driver.findElement(By.name("fromPort"));
		Select se=new Select(element);
		//se.selectByValue("Paris");
		//se.selectByIndex(7);
		se.selectByVisibleText("London");
		
		
		
		
		File scrFile2 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile2, new File("F:\\Palliumskills\\AutomationScreenshot\\screenshot04.png"));
		
		String expectedtitle= "Find a Flight: Mercury Tours:";
		String actualtitle= driver.getTitle();
		
		if(expectedtitle.equalsIgnoreCase(actualtitle))
		{
			System.out.println("PASS");
		}
		else
		{
			System.out.println("FAIL");
		}
	}
}