package com.mercury.test;

import java.awt.AWTException;
import java.io.IOException;

import org.sikuli.script.FindFailed;

public class Driver {

	public static void main(String[] args) throws InterruptedException, IOException, AWTException, FindFailed {
		MethodRepository obj1=new MethodRepository();
		obj1.mercuryAppLaunch();
		obj1.login();

	}

}
